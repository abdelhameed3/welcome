/*global $ */
/*eslint-env browser*/
/* Add active  class in home slider */
$(document).ready(function () {
  'use strict';
    
  $(".side-nav .control-side").click(function () {
      $(this).toggleClass("active");
      $('body').toggleClass("side-bar-icon");
  });
  $(".side-nav .control-side").click(function () {
      if( !$("body").hasClass("side-bar-icon" ) ){
          $(".main-panel").addClass("hide-phone");
      }else{
          $(".main-panel").removeClass("hide-phone");
      }
  });
  $('#accordionNav li a img').click(function (){
       if($("body").hasClass("side-bar-icon" )){
           $("body").removeClass("side-bar-icon" );
       }
  });
});
$(document).ready(function () {
  "use strict";
  function reStyle() {
      if ($(window).width() <= 770) {
          $('body').addClass("side-bar-icon");
      } else {
          $('body').removeClass("side-bar-icon");
      }
  }
  reStyle();
  $(window).resize(function () {
      reStyle();
  });
  
});
/*  alarts
============================== */
$(document).ready(function () {
    "use strict";
    /*=========================== Delete ============================== */
    $(".delete").click(function(){
        swal({
            title: "هل أنت متأكد من الحذف ؟",
            text: "بمجرد حذفها ، لن تكون قادراً على استرداد هذا  مرة اخري!",
            icon: "warning",
            buttons: true,
            dangerMode: true,

        })
        .then((willDelete) => {
             if (willDelete) {
                $(this).parent().parent().hide();
                 swal("حسناً! تم الحذف بنجاح", {
                     icon: "success",
                 });
             } else {
                 swal("حسناً! تم إلغاء عملية الحذف");
             }
         });
    });
    /*======================================= btn-toggle ===============================*/
    $(".btn-toggle").click(function (){
        swal({
            title: "هل أنت متأكد من تغير الحالة ؟",
            text: "هل توافق علي تغير الحالة؟ ",
            icon: "warning",
            buttons: true,
            dangerMode: true,

        })
        .then((willDelete) => {
            
             if (willDelete) {
                
                 $(this).toggleClass('active');
                 swal("حسناً! تم التغير بنجاح", {
                     icon: "success",
                 });
             } else {
                 swal("حسناً! تم إلغاء عملية تغير الحالة");
             }
         });
    });
    $('.v-confirm').click(function (){
        swal({
            text: "تم استلام بياناتك بنجاح .. سيتم اخطارك باستلام هديتك",
            icon: "success",
            buttons: false,

          });

    });
});

/* Add active To aide nav
======================== */
$(document).ready(function () {
    "use strict";
    $("#accordionNav li a").each(function () {
            var t = window.location.href.split(/[?#]/)[0];
            this.href == t && ($(this).addClass("active"), $(this).parent().parent().parent().parent().children('a').addClass("active"));
        })
});

/* Popup imag profile
========================= */
$(document).ready(function () {
  "use strict";
 $('table img.gift').click(function (){
     var getImg = $(this).attr('src');
     $('#img img').attr('src',getImg);
 });
 $('nav i').click(function (){
    $('.terms').toggleClass('active');
 });
 $('.put-active').click(function (){
    $('.view section').addClass('active');
 });


});
/* uplaod image
==================================== */

$(function () {

    // Viewing Uploaded Picture On Setup Admin Profile
    function livePreviewPicture(picture)
    {
      if (picture.files && picture.files[0]) {
        var picture_reader = new FileReader();
        picture_reader.onload = function(event) {
          $('#uploaded').attr('src', event.target.result);
        };
        picture_reader.readAsDataURL(picture.files[0]);
      }
    }
  
    $('.uplaod-img input').on('change', function () {
      $('#uploaded').fadeIn();
      livePreviewPicture(this);
    });
  
  });
  
  /* Count down
  ================================== */
  document.addEventListener("DOMContentLoaded", function() {
    let targetDate = new Date('June 18, 2019 13:00:00');
    let onStart = () => {
        document.querySelectorAll('.countdown-item').forEach(item => item.classList.add('show'))
    }
    let onTick = ({ days, hours, minutes, seconds }) => {
        document.querySelector('.countdown-item.days').innerHTML = days;
        document.querySelector('.countdown-item.hours').innerHTML = hours;
        document.querySelector('.countdown-item.minutes').innerHTML = minutes;
        document.querySelector('.countdown-item.seconds').innerHTML = seconds;
    };
    const sufixes = new LsCountdownSufixes({ days: ' :', hours: ' :', minutes: ' :', seconds: '' })


    let options = new LsCountdownOptions({ targetDate, onStart, onTick,sufixes });
    let countdown = new LsCountdown(options);

    countdown.start();
    window.mycountdown = countdown;
});
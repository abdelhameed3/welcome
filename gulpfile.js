const gulp = require('gulp');
const watch = require('gulp-watch');
const autoprefixer = require('gulp-autoprefixer');
const sass = require('gulp-ruby-sass');
const connect = require('gulp-connect');

// watch task


gulp.task('watch', function (){
   gulp.watch('./src/**/*.scss',['sass']); 
});



// sass task


gulp.task('sass',function(){
   sass('./src/sass/style.scss', {
       sourcemap: true,
       style: 'expanded'
   }) 
    .on('error', sass.logError)
    .pipe(autoprefixer({
        browsers: ['last 3 versions'],
        cascade: false
   }))
    .pipe(gulp.dest('assets/css'))
    .pipe(connect.reload());
});




// connect task


gulp.task('connect', function (){
   connect.server({
       port: 3333,
       root: './',
       livereload: true
   }); 
});



// Defult Task


gulp.task('default', [ 'connect', 'watch' ]);